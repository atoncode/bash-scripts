# Bash-Scripts

## Project status
The project is in development

## Description
The Bash-Scripts project aims to terraform GNU/Linux VMs with OS services, configurations and customization. Scripts will use the UNIX philosophy, meaning that each script has a unique purpose. Exist a main script called init.sh which will be the execution menu of all available brochure.

The project is organized in three main folders: Dependencies for *.sh, WellcomeOs for *.sh with OS customization purposes and Services for *.sh with proposals such as installing docker or initializing containers in docker already installed.


## Installation
Run
```bash
curl ……
```

## Usage
Only Run
```bash
sh init.sh
```
That enable the main menu. see the images

## Contributing
If you want to contribute to this project, the only thing you need to do is create a new branch respecting the file structure and following the existing encoding style. Create a pull request with the adjustment of your modifications or with the explanation of the new functionalities to the project. It will be jointly audited and approved in community. Do not forget to modify the readme in the authors' list

## Authors and acknowledgment
[Alejandro Sacristan Leal](https://twitter.com/AtonOverride)

## Support
If you whant support about that program you can to make a New Issue in the repository

## License

[MIT](https://choosealicense.com/licenses/mit/)
